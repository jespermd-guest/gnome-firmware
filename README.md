GNOME Firmware
==============

This application can:
* Upgrade, Downgrade, & Reinstall firmware on devices supported by fwupd.
* Unlock locked fwupd devices
* Verify firmware on supported devices
* Display all releases for a fwupd device

Screenshots:
![devices](contrib/screenshot.png)
![releases](contrib/screenshot2.png)
